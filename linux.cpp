#include "linux.h"
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/stat.h>

char* strupr(char *p) {
  while(*p)
  {
    *p=toupper( *p );
    p++;
  }
  return p;
}

char* strlwr(char *p) {
  while(*p)
  {
    *p=tolower( *p );
    p++;
  }
  return p;
}

void mkdir(char *p) {
  mkdir(p, 0770);
}

