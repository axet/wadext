#pragma once

#ifdef _WIN32

#include <windows.h>
#include <direct.h>
#include <io.h>

#else

#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned int
#define NULL 0

char* strupr(char *);
char* strlwr(char *);
void mkdir(char*);

#endif
